import React, {useState, useContext} from "react";
import axios from "axios";
import { Button, TextField } from '@material-ui/core';
import Alert from "@material-ui/lab/Alert";
import { ServiceUrlContext } from "./context";

const register = async (serviceUrl, userInfo, setError) => {
	try {
		await axios.post(`${serviceUrl}/register`, userInfo);
	} catch (e) {
		setError(e.message);
	};
};

let ShowErrors = ({message}) => {
	if (!message) {
		return "";
	}

	return <Alert severity="error">{message}</Alert>;
}

export default () => {
    let [email, setEmail] = useState();
	let [password, setPassword] = useState();
    let [error, setError] = useState();
    let serviceUrl = useContext(ServiceUrlContext);

    return (
        <div>
			<ShowErrors message={error}/>
            <div>
                <TextField label="Email address" onChange={(e) => setEmail(e.target.value)} type="phone"/>
            </div>
            <div>
                <TextField label="Password" onChange={(e) => setPassword(e.target.value)} type="text"/>
            </div>
            <div>
                <Button variant="contained" color="primary" onClick={async () => {
                    await register(serviceUrl, {
                        email,
                        password,
                    }, setError);
                }}>Register</Button>
            </div>
        </div>
    );
};