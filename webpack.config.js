const path = require("path");
const CopyPlugin = require("copy-webpack-plugin");
// const { DuplicatesPlugin } = require("inspectpack/plugin");

module.exports = {
    mode: "development",

    entry: {
        app: "./src/client/index.jsx"
    },

    devServer: {
        contentBase: path.join(__dirname, "build")
    },

    // Enable sourcemaps for debugging webpack's output.
    devtool: "source-map",

    plugins: [
        new CopyPlugin({
            patterns: [
                { from: './src/index.html' }
            ],
        }),
        // new DuplicatesPlugin({
        //     // Emit compilation warning or error? (Default: `false`)
        //     emitErrors: false,
        //     // Handle all messages with handler function (`(report: string)`)
        //     // Overrides `emitErrors` output.
        //     emitHandler: undefined,
        //     // List of packages that can be ignored. (Default: `[]`)
        //     // - If a string, then a prefix match of `{$name}/` for each module.
        //     // - If a regex, then `.test(pattern)` which means you should add slashes
        //     //   where appropriate.
        //     //
        //     // **Note**: Uses posix paths for all matching (e.g., on windows `/` not `\`).
        //     ignoredPackages: undefined,
        //     // Display full duplicates information? (Default: `false`)
        //     verbose: false
        // })
    ],

    resolve: {
        alias: {
            "~": path.resolve(__dirname, 'node_modules/'),
        },
        // Add '.ts' and '.tsx' as resolvable extensions.
        extensions: [".js", ".jsx"]
    },

    module: {
        rules: [{
            test: /\.js(x?)$/,
            exclude: /node_modules/,
            use: [
                {
                    loader: "ts-loader"
                }
            ]
        }, {
            // All output '.js' files will have any sourcemaps re-processed by 'source-map-loader'.
            enforce: "pre",
            test: /\.js$/,
            loader: "source-map-loader"
        }, {
            test: /\.scss$/i,
            use: ["style-loader", "css-loader", "sass-loader"],
        }, {
            test: /\.css$/i,
            use: ["style-loader", "css-loader"],
        }]
    },
};