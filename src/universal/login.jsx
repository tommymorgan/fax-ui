import React, {useState} from "react";
import { Button, TextField } from '@material-ui/core';
import Alert from "@material-ui/lab/Alert";
import { useLazyQuery } from "@apollo/client";
import gql from "graphql-tag";

let LOGIN_QUERY = gql`
    query ($email: String!, $password: String!) {
        login(email: $email, password: $password) {
            token
        }
    }
`;

let ShowErrors = ({message}) => {
	if (!message) {
		return "";
	}

	return <Alert severity="error">{message}</Alert>;
}

export default () => {
    let [email, setEmail] = useState("tommy@tommymorgan.com");
	let [password, setPassword] = useState("foo");
    const [getToken, tokenMeta] = useLazyQuery(LOGIN_QUERY, {
        onCompleted: (data) => {
            localStorage.setItem("authToken", data.login.token);
        },
        variables: {
            email,
            password,
        }
    });
    let buttonLabel = `Login`;

    return (
        <div>
			<ShowErrors message={tokenMeta.error}/>
            <div>
                <TextField label="Email address" defaultValue={email} onChange={(e) => setEmail(e.target.value)} type="phone"/>
            </div>
            <div>
                <TextField label="Password" defaultValue={password} onChange={(e) => setPassword(e.target.value)} type="text"/>
            </div>
            <div>
                <Button variant="contained" color="primary" onClick={() => {
                    getToken()
                }}>{buttonLabel}</Button>
            </div>
        </div>
    );
};