import React, {useState} from "react";
import axios from "axios";
import { Button, TextField } from '@material-ui/core';
import { ServiceUrlContext } from "./context";

const sendFax = async (serviceUrl, fax) => {
    await axios.post(`${serviceUrl}/fax`, fax);
};

export default () => {
    let [to, setTo] = useState();
    let [file, setFile] = useState();
    let serviceUrl = useContext(ServiceUrlContext);

    return (
        <div>
            <div>
                <TextField label='"To" fax number' onChange={(e) => setTo(e.target.value)} type="phone"/>
            </div>
            <div>
                <TextField label="File URL" onChange={(e) => setFile(e.target.value)} type="text"/>
            </div>
            <div>
                <Button variant="contained" color="primary" onClick={async () => {
                    sendFax(serviceUrl, {
                        to,
                        file,
                    });
                }}>Send</Button>
            </div>
        </div>
    );
};