import React, { useContext } from "react";
import ReactDOM from "react-dom";
import App from "../universal/app";
import Login from "../universal/login";
import { ServiceContext } from "../universal/context";
import { ApolloClient, ApolloProvider, InMemoryCache } from '@apollo/client';

let cachedClient;

let getClient = (service) => {
    if (!cachedClient) {
        cachedClient = new ApolloClient({
            uri: `${service.url}/graphql`,
            cache: new InMemoryCache(),
        });
    }

    return cachedClient;
};

let ContextualizedRoot = () => {
    let service = useContext(ServiceContext);
    let client = getClient(service);

    if (!client) {
        return "";
    }

    return (
        <ApolloProvider client={client}>
            <Login/>
        </ApolloProvider>
    );
};

let Root = () => {
    let service = useContext(ServiceContext);

    return (
        <ServiceContext.Provider value={service}>
            <ContextualizedRoot/>
        </ServiceContext.Provider>
    );
};

ReactDOM.render(
    <Root />,
    document.getElementById("app")
);
